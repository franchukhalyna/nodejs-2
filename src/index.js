const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');

const cors = require('cors');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://admin-galina:13579@cluster0.rfa6e.mongodb.net/notesapp?retryWrites=true&w=majority');

//const { filesRouter } = require('./filesRouter.js');
const { notesRouter } = require('./notesRouter.js');
const { usersRouter } = require('./usersRouter.js');
const { usersMeRouter } = require('./usersMeRouter.js');


app.use(express.static('public'));

app.use(cors());
app.use(cookieParser());

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/auth', usersRouter);
app.use('/api/users/me', usersMeRouter);

//app.use('api/auth', authRouter);

const start = async () => {
    app.listen(8080);
};

start();

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
