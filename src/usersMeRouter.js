const express = require('express');
const router = express.Router();
const { getUsersProfile, deleteUsersProfile, updateUsersProfile } = require('./usersMeService.js');
const { middleware } = require('./middleware/middleware');


router.get('/', middleware, getUsersProfile);

router.patch('/', middleware, updateUsersProfile);

router.delete('/', middleware, deleteUsersProfile);


module.exports = {
  usersMeRouter: router,
};
