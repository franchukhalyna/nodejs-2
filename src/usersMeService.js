const { User } = require('./models/Users.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const getUsersProfile = (req, res, next) => {
  User.findById(req.user.userId, function(err, user) {
    if (err) {
      res.status(200).json({'message': 'User not found'});
    } else {
      const result = {"_id": req.user.userId, "username": user.username, "createdDate": user.createdDate };
      res.status(200).json({"user": result});
    }
  })
} 

const deleteUsersProfile = (req, res, next) => {
  User.findByIdAndDelete(req.user.userId, function(err, user) {
    if (err || !user) {
      res.status(200).json({'message': 'User not found'});
    } else {
      res.status(200).json({"message": "success. user removed."});
    }
  })
}

const updateUsersProfile = (req, res, next) => {
  if(Object.keys(req.body).length===0) {
    res.status(400).json({'message': 'Invalid parameters'});
  } else {
    const { oldPassword, newPassword } = req.body;
    console.log(req.user.userId);
    return User.findByIdAndUpdate({_id: req.user.userId, userId: req.user.userId}, {$set: { password: newPassword } }, function(err) {
      if(err) {
        res.status(200).json({'message': 'Error'});
      } else {
        res.status(200).json({'message': 'Password was updated'});
      }
    })
  }


  User.findByIdAndUpdate(req.user.userId, function(err, user) {
    if (err || !user) {
      res.status(200).json({'message': 'User not found'});
    } else {
      res.status(200).json({"message": "success. user removed."});
    }
  })
}



module.exports = {
  getUsersProfile,
  deleteUsersProfile,
  updateUsersProfile
};
