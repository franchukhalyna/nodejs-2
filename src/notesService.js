const { Note } = require('./models/Notes.js');
const jwt = require('jsonwebtoken');


function createNote(req, res, next) {
  
  if(Object.keys(req.body).length===0) {
    res.status(400).json({'message': 'Invalid parameters'});
  } else {
    let { text, completed } = req.body;
    if(!completed) {
      completed = false;
    }

      const userId = req.user.userId;
      const createdDate = JSON.stringify(new Date()).replace(/['"]+/g, '');
      console.log(createdDate);
      const note = new Note({
        text,
        userId,
        completed,
        createdDate
      });
      note.save().then((saved) => {
        //res.json({saved});
        res.json({'message': 'Success'});
      });
  } 
}

const getNote = (req, res, next) => {
  Note.findById(req.params.id, function(err, note) {
    if (err || !note) {
      res.status(200).json({"note": {'message': 'Id not found'}});
    } else {
      res.status(200).json({"note": note});
    }
  })  
} 

const deleteNote = (req, res, next) => {
  Note.findByIdAndDelete(req.params.id, function(err, note) {
    if (err || !note) {
      res.status(200).json({'message': 'Id not found'});
    } else {
      res.status(200).json({'message': 'Success'});
    }
  })
} 

function getNotes(req, res, next) {
  return Note.find({}, function(err, result) {
    let finalRes;
    let count = result.length;
    if (err) throw err;
    if(req.query.offset && req.query.limit) {
      finalRes = result.splice(req.query.offset, req.query.limit)
    } else if(req.query.limit) {
      finalRes = result.splice(0, req.query.limit);
    } else if(req.query.offset) {
      finalRes = result.splice(req.query.offset);      
    } else {
      finalRes = result.splice(0,1);
    }
    res.status(200).json({"offset": req.query.offset || 0, "limit": req.query.limit || 0,
    "count": count || 0, "notes": finalRes || result});
  });  
}

const updateMyNoteById = (req, res, next) => {
  if(Object.keys(req.body).length===0) {
    res.status(400).json({'message': 'Invalid parameters'});
  } else {
    const { text, completed } = req.body;
    return Note.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { text, completed } }, function(err) {
      if(err) {
        res.status(200).json({'message': 'Error'});
      } else {
        res.status(200).json({'message': 'Note was updated'});
      }
    })
  }
}

const markMyNoteCompletedById = (req, res, next) => {

  Note.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { completed: true } }, function(err) {
    if (err) {
      res.status(200).json({'message': 'Id not found'});
    } else {
      res.status(200).json({'message': 'Success'});
    }
  })

}

module.exports = {
  createNote,
  getNote,
  getNotes,
  deleteNote,
  updateMyNoteById,
  markMyNoteCompletedById
};
