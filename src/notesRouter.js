const express = require('express');
const router = express.Router();
const {
  createNote, getNote, getNotes, deleteNote, updateMyNoteById,
  markMyNoteCompletedById
} = require('./notesService.js');
const { authMiddleware } = require('./middleware/authMiddleware');
const { middleware } = require('./middleware/middleware');


router.post('/', middleware, createNote);

router.get('/', getNotes);

router.get('/:id?', getNote);

router.put('/:id?', middleware, updateMyNoteById);

router.patch('/:id?', authMiddleware, markMyNoteCompletedById);

router.delete('/:id?', deleteNote);

module.exports = {
  notesRouter: router,
};
