const express = require('express');
const router = express.Router();
const { registerUser, loginUser } = require('./usersService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/register', registerUser);

router.post('/login', loginUser);
//router.get('/login', sendToken);

module.exports = {
  usersRouter: router,
};
