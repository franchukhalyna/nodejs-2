const { User } = require('./models/Users.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerUser = async (req, res, next) => {
  if(Object.keys(req.body).length===0 || !req.body.username || !req.body.password) {
    res.status(400).send({ "message": "Please specify right parameters" });
  } else {
    const { name, username, password } = req.body;
    const createdDate = JSON.stringify(new Date()).replace(/['"]+/g, '');
    const user = new User({
      name,
      username,
      password: await bcrypt.hash(password, 10),
      createdDate
    });

    user.save()
      .then(saved => {
        // res.json(saved);
        res.status(200).json({'message': 'Success'})
      })
      .catch(err => {
        next(err);
      });
  }
  
}

const loginUser = async (req, res, next) => {
  if(!req.body.username || !req.body.password) {
    return res.status(200).json({'message': 'Wrong param'});
  } else {
    const user = await User.findOne({ username: req.body.username });
    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { username: user.username, name: user.name, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res
    .cookie("access_token", jwtToken)
    .status(200)
    .json({ "message": "Success", jwt_token: jwtToken});
    } else if(!user) {
      return res.status(400).json({'message': 'User not find'});
    }  
  }
  return res.status(400).json({'message': 'Err'});
}

module.exports = {
  registerUser,
  loginUser
};
